[![DeepSource](https://deepsource.io/gh/KOSASIH/Metazone.svg/?label=active+issues&show_trend=true&token=OUldJiBXyyndWIItWLe5QOm9)](https://deepsource.io/gh/KOSASIH/Metazone/?ref=repository-badge)
[![Documentation Status](https://readthedocs.org/projects/metazone/badge/?version=latest)](https://metazone.readthedocs.io/en/latest/?badge=latest)
[![Netlify Status](https://api.netlify.com/api/v1/badges/94d79b95-b047-4063-bd88-97d455a2d699/deploy-status)](https://app.netlify.com/sites/metazone/deploys)     
[![CircleCI](https://circleci.com/gh/KOSASIH/Metazone/tree/circleci-project-setup.svg?style=svg)](https://circleci.com/gh/KOSASIH/Metazone/tree/circleci-project-setup)
[![GitHub issues](https://img.shields.io/github/issues/KOSASIH/Metazone)](https://github.com/KOSASIH/Metazone/issues)
[![GitHub forks](https://img.shields.io/github/forks/KOSASIH/Metazone)](https://github.com/KOSASIH/Metazone/network)
[![GitHub stars](https://img.shields.io/github/stars/KOSASIH/Metazone)](https://github.com/KOSASIH/Metazone/stargazers)
[![GitHub license](https://img.shields.io/github/license/KOSASIH/Metazone)](https://github.com/KOSASIH/Metazone/blob/main/LICENSE)
[![Twitter](https://img.shields.io/twitter/url?style=social&url=https%3A%2F%2Ftwitter.com%2FKosasihg88G)](https://twitter.com/intent/tweet?text=Wow:&url=https%3A%2F%2Ftwitter.com%2FKosasihg88G)
[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/5495/badge)](https://bestpractices.coreinfrastructure.org/projects/5495)


# Metazone
NFT Marketplace which provides transactions with cryptocurrencies
